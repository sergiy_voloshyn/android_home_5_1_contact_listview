package com.xyz.android_home_5_1;

import android.media.Image;
import android.widget.ImageView;

/**
 * Created by user on 17.01.2018.
 */
/*
Каждый контакт содержит имя, email, адрес и
телефон иконку (или фотку)
 */

public class Contact {
    private String name;
    private String email;
    private String address;
    private String phone;
    private Integer photo;

    public Contact(String name, String email, String address, String phone, Integer photo) {
        this.name = name;
        this.email = email;
        this.address = address;
        this.phone = phone;
        this.photo = photo;
    }

    public String getName() {
        return name;
    }

    public String getEmail() {
        return email;
    }

    public String getAddress() {
        return address;
    }

    public String getPhone() {
        return phone;
    }

    public Integer getPhoto() {
        return photo;
    }
}
