package com.xyz.android_home_5_1;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.ButterKnife;

import static com.xyz.android_home_5_1.ListActivity.ADDRESS;
import static com.xyz.android_home_5_1.ListActivity.EMAIL;
import static com.xyz.android_home_5_1.ListActivity.NAME;
import static com.xyz.android_home_5_1.ListActivity.PHONE;

public class InfoActivity extends AppCompatActivity {


    @BindView(R.id.name)
    TextView viewName;
    @BindView(R.id.email)
    TextView viewEmail;
    @BindView(R.id.phone)
    TextView viewPhone;
    @BindView(R.id.address)
    TextView viewAddress;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_info);
        ButterKnife.bind(this);


        viewName.setText(getIntent().getExtras().getString(NAME));
        viewEmail.setText(getIntent().getExtras().getString(EMAIL));

        viewPhone.setText(getIntent().getExtras().getString(PHONE));
        viewAddress.setText(getIntent().getExtras().getString(ADDRESS));

    }
}
