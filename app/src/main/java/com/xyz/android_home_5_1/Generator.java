package com.xyz.android_home_5_1;

import android.graphics.drawable.Drawable;
import android.media.Image;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Created by user on 17.01.2018.
 */

public final class Generator {

    public Generator() {
    }

    public static Contact[] generate() {
        Contact[] contacts = new Contact[11];
        Integer[] images = {
                R.drawable.image_1,
                R.drawable.image_2,
                R.drawable.image_3,
                R.drawable.image_4,
                R.drawable.image_5,
                R.drawable.image_6,
                R.drawable.image_7,
                R.drawable.image_8,
                R.drawable.image_9,
                R.drawable.image_10,
                R.drawable.image_11,
                R.drawable.image_12
        };

        String photoName = "R.drawable.image_";

        for (int i = 0; i < contacts.length; i++) {

            contacts[i] = new Contact("name " + i, "email " + i, "address " + i,
                    "phone " + i, images[i]);
        }

        return contacts;
    }

    public static List<Contact> generateList() {

        return new ArrayList<Contact>(Arrays.asList(generate()));
    }


}
