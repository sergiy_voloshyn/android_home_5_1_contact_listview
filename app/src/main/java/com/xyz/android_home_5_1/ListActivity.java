package com.xyz.android_home_5_1;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.LayoutRes;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.OnItemClick;
/*

1. Создать список контактов. Каждый контакт содержит имя, email, адрес и
телефон иконку (или фотку). Список содержит только иконку, имя и email контакта.
При нажатии на контакт - он должен открываться в новом activity с подробной
 о нем информацией. Использовать ListView.
//first activity
Intent mIntent = new Intent(this, SecondActivity.class);
mIntent.putExtra(key, value); //добавляем в intent значение и ключ к нему
//second activity
String value = getIntent().getExtras().getString(key); //получаем добавленное значение,
указав ключ
1.1* Добавить возможность добавлять контакты в список на первом экране.

 */

public class ListActivity extends AppCompatActivity {

    public static final String NAME = "NAME";
    public static final String EMAIL = "EMAIL";
    public static final String PHONE = "PHONE";
    public static final String ADDRESS = "ADDRESS";

    ListView listView;
    ContactAdapter adapter;
    // Contact[] contacts = Generator.generate();
    List<Contact> contactList = Generator.generateList();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list);
        ButterKnife.bind(this);

        adapter = new ContactAdapter(this, contactList);
        listView.setAdapter(adapter);

        listView.setOnItemClickListener(
                new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> adapterView, View view, int position, long id) {

                        //START new activity
                        Intent showInfo = new Intent(ListActivity.this, InfoActivity.class);

                        showInfo.putExtra(NAME, contactList.get(position).getName());
                        showInfo.putExtra(EMAIL, contactList.get(position).getEmail());
                        showInfo.putExtra(PHONE, contactList.get(position).getPhone());
                        showInfo.putExtra(ADDRESS, contactList.get(position).getAddress());

                        startActivity(showInfo);

                        //Toast.makeText(ListActivity.this, contacts[position].getName(), Toast.LENGTH_SHORT).show();
                    }
                }

        );

    }


    @OnClick(R.id.buttonAdd)
    public void onClickButton() {
        contactList.add(new Contact("name", "email", "address", "phone", R.drawable.image_9));
        adapter.notifyDataSetChanged();
    }


    class ContactAdapter extends ArrayAdapter<Contact> {

        @BindView(R.id.photo)
        ImageView photo;
        @BindView(R.id.name)
        TextView name;
        @BindView(R.id.email)
        TextView email;

        public ContactAdapter(@NonNull Context context, List<Contact> contacts) {
            super(context, R.layout.activity_list, contacts);
            listView = (ListView) findViewById(R.id.list_view);


        }

        @NonNull
        @Override
        public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
            View root = getLayoutInflater().inflate(R.layout.item_layout, parent, false);

            ButterKnife.bind(this, root);
            Contact contact = getItem(position);
            photo.setImageResource(contact.getPhoto());
            name.setText(contact.getName());
            email.setText(contact.getEmail());

            return root;


        }
    }
}
